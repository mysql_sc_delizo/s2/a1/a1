-- List down the databases in the DBMS
SHOW DATABASES;

-- Create a database
CREATE DATABASE blog_db;


-- create table users
CREATE TABLE users
(
  id INT NOT NULL
  AUTO_INCREMENT,
  email VARCHAR
  (100),
  password VARCHAR
  (300),
  datetime_created DATE NOT NULL,
    PRIMARY KEY
  (id)
);


  -- create user posts

  CREATE TABLE posts
  (
    id INT NOT NULL
    AUTO_INCREMENT,
    datetime_created DATE NOT NULL,
    PRIMARY KEY
    (id),
    title VARCHAR
    (500),
    content VARCHAR
    (5000),
    author_id INT NOT NULL,
     datetime_posted DATE NOT NULL,
    CONSTRAINT fk_POST_author_id
        FOREIGN KEY
    (author_id) REFERENCES users
    (id)
    ON
    UPDATE CASCADE
    ON
    DELETE RESTRICT
);



    -- create post_likes


    CREATE TABLE post_likes
    (
      id INT NOT NULL
      AUTO_INCREMENT,
    datetime_like DATE NOT NULL,
    post_id INT NOT NULL,
    user_id INT NOT NULL,
        PRIMARY KEY
      (id),
    CONSTRAINT fk_POST_user_id
        FOREIGN KEY
      (user_id) REFERENCES users
      (id)
        ON
      UPDATE CASCADE
        ON
      DELETE RESTRICT,
   CONSTRAINT fk_POST_post_id
        FOREIGN KEY
      (post_id) REFERENCES posts
      (id)
        ON
      UPDATE CASCADE
        ON
      DELETE RESTRICT     
);

      CREATE TABLE post_comments
      (
        id INT NOT NULL
        AUTO_INCREMENT,
    datetime_commented DATE NOT NULL,
    content VARCHAR
        (5000),
    post_id INT NOT NULL,
    user_id INT NOT NULL,
        PRIMARY KEY
        (id),
    CONSTRAINT fk_COMMENTS_user_id
        FOREIGN KEY
        (user_id) REFERENCES users
        (id)
        ON
        UPDATE CASCADE
        ON
        DELETE RESTRICT,
   CONSTRAINT fk_COMMENTS_post_id
        FOREIGN KEY
        (post_id) REFERENCES posts
        (id)
        ON
        UPDATE CASCADE
        ON
        DELETE RESTRICT     
);